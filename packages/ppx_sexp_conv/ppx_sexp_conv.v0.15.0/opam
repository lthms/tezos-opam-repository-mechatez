opam-version: "2.0"
maintainer: "Jane Street developers"
authors: ["Jane Street Group, LLC"]
homepage: "https://github.com/janestreet/ppx_sexp_conv"
bug-reports: "https://github.com/janestreet/ppx_sexp_conv/issues"
dev-repo: "git+https://github.com/janestreet/ppx_sexp_conv.git"
doc: "https://ocaml.janestreet.com/ocaml-core/latest/doc/ppx_sexp_conv/index.html"
license: "MIT"
build: [
  ["dune" "build" "-p" name "-j" jobs]
]
depends: [
  "ocaml"    {>= "4.08.0"}
  "base"     {>= "v0.15" & < "v0.16"}
  "sexplib0" {>= "v0.15" & < "v0.16"}
  "dune"     {>= "2.0.0"}
  "ppxlib"   {>= "0.23.0" & < "0.26.0"}
]
synopsis: "[@@deriving] plugin to generate S-expression conversion functions"
description: "
Part of the Jane Street's PPX rewriters collection.
"
url {
  src:
    "https://ocaml.janestreet.com/ocaml-core/v0.15/files/ppx_sexp_conv-v0.15.0.tar.gz"
  checksum: [
    "sha256=d9cd1eefa179acedb8954ba95ed01e8fd685dae6e278061936ce5930d95a8380"
    "sha512=b4dd4e0b6c3fcb2d6d258f766bf7a2e889ab82d6263112fc10e52f08674db3641131c11f15d3c19c0da6e5679e53e445da6104043618390c1e8184167756e877"
  ]
}
